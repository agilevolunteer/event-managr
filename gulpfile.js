var gulp = require('gulp');
var wpPot = require('gulp-wp-pot');
var sort = require('gulp-sort');
var gettext = require('gulp-gettext');
var pseudo = require('gulp-pseudo-i18n');
var stylus = require('gulp-stylus');
var concat = require('gulp-concat');
var jade = require('gulp-jade');
var rev = require('gulp-rev');
var rimraf = require('rimraf');
var babel = require('gulp-babel');
var ngAnnotate = require('gulp-ng-annotate');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var browserSync = require('browser-sync');
var yaml = require('gulp-yaml');
var gif = require('gulp-if');
var sourcemaps = require('gulp-sourcemaps');
var elm = require("gulp-elm");
var reload = browserSync.reload;

plumberConf = {
  errorHandler: notify.onError("Error: <%= error.message %>")
};

gulp.task('sync', function() {
  browserSync({
    proxy: 'http://eventmanagr.dev',
    port: 7770,
    open: false,
    middleware: function (req, res, next) {
            //console.log('Adding CORS header for ' + req.method + ': ' + req.url);
            res.setHeader('Access-Control-Allow-Origin', '*');
            next();
        }
  });
});

gulp.task('fixtures', function() {
  gulp.src('./app/fixtures/**/*.yml')
  .pipe(plumber(plumberConf))
  .pipe(yaml({ schema: 'DEFAULT_SAFE_SCHEMA' }))
  .pipe(gulp.dest('./app/fixtures'));
});

gulp.task("fixtures:watch", ["fixtures"], reload);


gulp.task('serve', function() {
  browserSync({
    server: "app",
    files: "app/**/*.{html,js,css,json}",
    middleware: function (req, res, next) {
        if (req.url.indexOf("fixtures/api") > -1){
            bla = req.url + "_" + req.method + ".json";
            req.url = bla;
        }
        console.log(req.method, ": ", req.url);
        next();
    }
  });
});

gulp.task('i18n', function () {
    return gulp.src('src/**/*.php')
        .pipe(plumber(plumberConf))
        .pipe(sort())
        .pipe(wpPot( {
            domain: 'wp-event-managr',
            destFile:'wp-event-managr.pot',
            package: 'wp-event-managr',
            lastTranslator: 'Marcel Henning <marcel@agilevolunteer.com>',
        } ))
        .pipe(gulp.dest('languages'));
});

gulp.task('gettext', function() {
  gulp.src('languages/**/*.po')
    .pipe(plumber(plumberConf))
    .pipe(gettext())
    .pipe(gulp.dest('languages/mo'))
  ;
});

gulp.task('pseudo', function () {
  return gulp.src('languages/**/*.pot')
    .pipe(plumber(plumberConf))
    .pipe(pseudo({
      language: "de_DE",
    }))
    .pipe(gulp.dest('languages/'));
});

gulp.task("styles", [], function () {
  return gulp.src(["./app/styl/**/*.styl", "app/styl/vendor/**/*.min.css", "app/styl/vendor/ngtoast.css"])
    .pipe(plumber(plumberConf))
    .pipe(gif(/[.]styl$/,stylus({
      paths: ["./app/styl/lib"],
      import: ["variables", "mediaQueries"]
    })))
    .pipe(concat("event-managr.css"))
    .pipe(gulp.dest("./app/styles"))
    .pipe(reload({stream: true}));
});

gulp.task("views", [], function(){
  return gulp.src(["./app/jade/**/*.jade"])
    .pipe(plumber(plumberConf))
    .pipe(jade())
    .pipe(gulp.dest("./app/views"));
});

gulp.task("views:dev", [], function(){
  return gulp.src(["./app/*.jade"])
    .pipe(plumber(plumberConf))
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest("./app"));
});

gulp.task("views:watch", ["views", "views:dev"], reload);

gulp.task("scripts:vendor", [], function () {
  return gulp.src(["bower_components/angular/angular.min.js", "bower_components/**/*.min.js"])
    .pipe(plumber(plumberConf))
    .pipe(concat("vendor.js"))
    .pipe(gulp.dest("./app/scripts"));
});
gulp.task("scripts:app", [], function () {
  return gulp.src(["./app/modules/**/*App.js", "./app/modules/**/*.js"])
    .pipe(plumber(plumberConf))
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(concat("event-managr.js"))
    .pipe(ngAnnotate())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest("./app/scripts"))
    .pipe(reload({stream: true}));;
});

gulp.task("rev", ["scripts:vendor", "scripts:app", "styles"], function() {
  return gulp.src("app/{scripts,styles}/*.{css,js}")
    .pipe(plumber(plumberConf))
    .pipe(rev())
    .pipe(gulp.dest("app"))
    .pipe(rev.manifest())
    .pipe(gulp.dest("app"))
    .pipe(reload({stream: true}));
});

gulp.task("dist", function(){
  gulp.src(["./{src,templates,languages,app}/**/*.{php,js,css,po,mo,html,json}","./*.php"])
  .pipe(gulp.dest("dist"))
});

gulp.task("clean", ['cleandist'], function (cb) {
  rimraf("./app/{scripts,styles,views,rev-manifest.json,index.html}", cb);
});
gulp.task("cleandist", function (cb) {
  rimraf("./dist", cb);
});

gulp.task('elm-init', elm.init);

gulp.task('elm', ['elm-init'], function(){
  return gulp.src(['timetable/Timetable.elm'])
    .pipe(plumber(plumberConf))
    .pipe(elm())
    .pipe(gulp.dest('app/scripts/'))
    .pipe(reload({stream: true}));
});

gulp.task("watch", function(){
  gulp.watch("app/styl/**/*.styl", ["styles"]);
  gulp.watch("app/jade/**/*.jade", ["views:watch"]);
  gulp.watch("app/*.jade", ["views:watch"]);
  gulp.watch("app/modules/**/*.js", ["scripts:app"]);
  gulp.watch("timetable/**/*.elm", ["elm"]);
  gulp.watch("app/fixtures/**/*.yml", ["fixtures:watch"]);
});
gulp.task("scripts", ["scripts:vendor", "scripts:app"]);
gulp.task("default", ["views", "rev", "dist"]);
gulp.task("build", ["scripts", "styles", "views", "elm"]);
gulp.task("dev", ["develop"]);
gulp.task("develop", ["build", "watch", "views:dev", "fixtures"], function(){
  browserSync({
    server: "app/",
    middleware: function (req, res, next) {
        if (req.url.indexOf("fixtures/api") > -1){
            bla = req.url + "_" + req.method + ".json";
            req.url = bla;
        }
        if (req.method == "POST"){
          var parsed = require("./app/fixtures/api/users_POST.json");
          res.setHeader("Content-Type", "application/json");
          res.end(JSON.stringify(parsed));
        }
        console.log(req.method, ": ", req.url);
        next();
    }
  });
});
