// spec.js
describe('Protractor Demo App', function() {
    beforeEach(function(){
        isAngularSite(false);
    });

    it('should have a title', function() {
        browser.get('');


        expect(browser.getTitle()).toEqual('Vagrant | Just another WordPress site');
    });
});