LostPasswordPage = require("./lostPassword_page")

describe "LostPassword", ->
  page = null
  beforeEach ->
    isAngularSite(false)
    page = new LostPasswordPage()
    page.get()

  it "should be able to enter invalid username", ->
    console.log page
    page.typeUser("klausbaerbel")
    page.submit()
    expect(page.loginError().count()).toEqual(1)

