class LostPasswordPage
  get: ->
    browser.get("/wp-login.php?action=lostpassword")
    isAngularSite(false)

  typeUser: (text) ->
    element(By.css("#user_login")).sendKeys(text)

  submit: ->
    element(By.css("#wp-submit")).click()

  loginError: ->
    element.all(By.css("#login_error"))

module.exports = LostPasswordPage