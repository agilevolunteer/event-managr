describe 'Protractor Demo App', ->
  beforeEach ->
    isAngularSite false
    return
  it 'should have a title', ->
    browser.get ''
    expect(browser.getTitle()).toEqual 'Vagrant | Just another WordPress site'
    return
  return