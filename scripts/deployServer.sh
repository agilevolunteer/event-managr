#!/usr/bin/env bash
while getopts s:p:t: option
do
  case "${option}"
  in
  s) SERVER=${OPTARG};;
  p) HASH=${OPTARG};;
  t) TOKEN=${OPTARG};;
esac
done

echo "calling deploy to $SERVER"
echo "https://semaphoreci.com/api/v1/projects/$HASH/$BRANCH_NAME/builds/$SEMAPHORE_BUILD_NUMBER/deploy/$SERVER/?auth_token=$TOKEN"
curl --data "auth_token=$TOKEN" https://semaphoreci.com/api/v1/projects/$HASH/$BRANCH_NAME/builds/$SEMAPHORE_BUILD_NUMBER/deploy/$SERVER
