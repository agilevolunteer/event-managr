require("coffee-script/register");

// conf.js
exports.config = {
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    seleniumServerJar: "scripts/selenium-server.jar",

    specs: ['e2e/**/*_spec.coffee'],
    capabilities: {
        "browserName": "firefox"
    },
    onPrepare: function(){
        global.isAngularSite = function(flag){
            browser.ignoreSynchronization = !flag;
        };
    },
    baseUrl: "http://localhost:7777"
}