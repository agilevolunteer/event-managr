/**
 * @ngdoc service
 * @name workshopRegApp.UserDataService
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $replace_me
 *
 * */

'use strict';
angular.module('agv.common')
    .service('UserDataService', ['$resource', 'APP_SETTINGS', function ($resource, settings) {

        return $resource(settings.api + 'users/:id',
            {
                id: '@id'
            }, {
                put: {
                    method: 'POST',
                    headers: {
                        'X-WP-NONCE': settings.nonce
                    },
                    cache: false
                }
            });
    }]);
