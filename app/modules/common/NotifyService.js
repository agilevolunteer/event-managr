'use strict';

angular.module('agv.common')
  .service('NotifyService', ['ngToast', (ngToast) => {
    return {
      warn: (message) => {
        ngToast.warning({
          content: message,
          dismissButton: true
        });
      },
      success: (message) => {
        ngToast.success({
          content: message,
          dismissButton: true
        });
      },
      failed: (message) => {
        ngToast.danger({
          content: message,
          dismissButton: true
        });
      }
    };
  }]);
