angular.module('workshopRegApp')
    .controller('workshopDataCtrl', ['$scope', 'WorkshopDataService', '$location', 'UserDataService', '$resource', 'NotifyService', function ($scope, WorkshopDataService, $location, UserDataService, $resource, NotifyService) {
        /**
         * @ngdoc controller
         * @name workshopRegApp.controller:workshopDataCtrl
         *
         * @description
         * _Please update the description and dependencies._
         *
         * @requires $scope
         * */
        if (WorkshopDataService.user === undefined) {
            $location.path('');
            return;
        }

        var workshop = function () {
            this.title = '';
            this.content_raw = '';
            this.excert_raw = '';
            this.type = 'workshops';
            this.status = 'pending';
            this.terms = {
                times: [],
                topics: [],
                methods: []
            };
            this.customFields = {
                'RaumAnforderungen': '',
                'InterneBeschreibung': '',
                'MaxTeilnehmer': '',
                'MaterialWunsch': '',
                'Feedback': '',
                'AnzahlDurchfuehrung': 1
            };


        }

        $scope.messages = [];
        $scope.workshopToEdit = new workshop();

        $scope.workshops = WorkshopDataService.workshops.query({
            "filter[author]": WorkshopDataService.user.ID,
            "type": 'workshops',
            "filter[post_status][0]": 'pending',
            "filter[post_status][1]": 'draft',
            "filter[post_status][2]": 'publish'
        });

        $scope.times = WorkshopDataService.taxonomies.query({
            tax: 'times'
        });

        $scope.toggleItem = function(item, haystack){
            var index = haystack.indexOf(item);
            if(index !== -1){
                haystack.splice(index, 1);
            } else {
                haystack.push(item);
            }
        };

        $scope.hasItem = function(item, haystack){
            if(haystack.indexOf(item) === -1){
                return false;
            }

            return true;
        };

        $scope.methods = WorkshopDataService.taxonomies.query({
            tax: 'methods'
        });

        $scope.topics = WorkshopDataService.taxonomies.query({
            tax: 'topics'
        });


        function mapUserToSCope() {
            $scope.userID = WorkshopDataService.user.ID;
            $scope.firstName = WorkshopDataService.user.first_name;
            $scope.lastName = WorkshopDataService.user.last_name;
            $scope.zip = WorkshopDataService.user.meta.zip;
            $scope.city = WorkshopDataService.user.meta.city;
            $scope.hasWorkshopRegistration = WorkshopDataService.user.eventManagr.hasWorkshopRegistration;
            $scope.phone = WorkshopDataService.user.meta.phone;
            $scope.job = WorkshopDataService.user.meta.job;
            $scope.church = WorkshopDataService.user.meta.church;
            $scope.churchContact = WorkshopDataService.user.meta.churchContact;
            $scope.experience = WorkshopDataService.user.meta.experience;
            $scope.community_optin = (WorkshopDataService.user.meta.community_optin == 1);

            if ($scope.hasWorkshopRegistration === false) {
                $scope.activeTab = 1;
            } else {
                $scope.activeTab = 0;
            }
        }

        $scope.saveUser = function () {
            WorkshopDataService.user.first_name = $scope.firstName;
            WorkshopDataService.user.last_name = $scope.lastName;
            WorkshopDataService.user.meta.zip = $scope.zip;
            WorkshopDataService.user.meta.city = $scope.city;
            WorkshopDataService.user.meta.phone = $scope.phone;
            WorkshopDataService.user.meta.job= $scope.job;
            WorkshopDataService.user.meta.church= $scope.church;
            WorkshopDataService.user.meta.churchContact= $scope.churchConact;
            WorkshopDataService.user.meta.experience= $scope.experience;
            WorkshopDataService.user.meta.community_optin= $scope.community_optin;


            UserDataService.put({
                id: $scope.userID,
                first_name: $scope.firstName,
                last_name: $scope.lastName,
                zip: $scope.zip,
                volunteerContext: 'workshop',
                phone: $scope.phone,
                job: $scope.job,
                church: $scope.church,
                churchContact: $scope.churchContact,
                experience: $scope.experience,
                community_optin: $scope.community_optin

            }, function (data) {
                if (WorkshopDataService.user.eventManagr.hasWorkshopRegistration === false && data.eventManagr.hasWorkshopRegistration === true) {
                    $scope.messages.push('Die Anmeldung an den Workshops war erfolgreich.');
                    NotifyService.success('Die Anmeldung an den Workshops war erfolgreich.');
                }
                WorkshopDataService.user = data;
                $scope.messages.push('Deine Daten wurden erfolgreich gespeichert.');
                NotifyService.success('Deine Daten wurden erfolgreich gespeichert.')
                setTimeout(function () {
                    $scope.messages = [];
                    $scope.$digest();
                }, 1500);
                mapUserToSCope();
            }, function (response) {
                $scope.errors = response.data;
                NotifyService.failed(response.data);
                setTimeout(function () {
                    $scope.errors = [];
                    $scope.$digest();
                }, 1500);
            });
        };

        $scope.addMetaToWorkshop = function(postId){
            console.log("post", postId);
            //TODO: should been doing this in a service I know
            var customFields = $resource(settings.api + 'posts/'+ postId +'/meta',{},{
                save: {
                    method: "POST",
                    headers: {
                        "X-WP-NONCE": settings.nonce
                    },
                    cache: false
                }
            });

            angular.forEach($scope.workshopToEdit.customFields, function(value, key){
                customFields.save({
                    key: key,
                    value: value
                });
            });
        };

        $scope.saveWorkshopToEdit = function () {
            var editWorkshop = WorkshopDataService.workshops.save($scope.workshopToEdit).$promise;

            editWorkshop.then(function (data) {
                $scope.addMetaToWorkshop(data.ID)

                $scope.messages.push('Der Workshop wurde erfolgreich angelegt.');
                NotifyService.success('Der Workshop wurde erfolgreich eingereicht.');
                $scope.workshopToEdit = new workshop();

                $scope.workshops.push(data);
                setTimeout(function () {
                    $scope.messages = [];
                    $scope.$digest();
                }, 1500);
            });

            editWorkshop.catch(function(response){
                $scope.errors = response.data;
                NotifyService.failed(response.data);
                setTimeout(function () {
                    $scope.errors = [];
                    $scope.$digest();
                }, 1500);
            });
        }

        mapUserToSCope();
    }]);
