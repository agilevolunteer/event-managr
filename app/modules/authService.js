workshopRegApp.factory('authService', ['$resource', 'APP_SETTINGS', function ($resource, settings) {
    return $resource(settings.api + 'eventmanagr/people/me', {}, {
        get: {
            method: "GET",
            headers: {
                "X-WP-NONCE": settings.nonce
            }
        },
        post: {
            method: "POST"
        }
    });
}]);
