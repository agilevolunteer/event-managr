workshopRegApp.controller('authController', ['$scope', 'authService', '$location', 'WorkshopDataService', '$window', 'registrationService', function ($scope, authService, $location, WorkshopDataService, $window, registrationService) {
    $scope.name = "";
    $scope.pw = "";
    $scope.errors = [];
    $scope.isAuthenticationRequired = false;

    function checkAuthentication() {
        authService.get(
            {},
            function (data) {
                WorkshopDataService.user = data;
                $location.path('data');
            },
            function (response) {
                if (response.status === 401) {
                    $scope.message = "Nicht eingeloggt";
                } else {
                    $scope.message = "Anderer Fehler";
                }

                $scope.isAuthenticationRequired = true;
            });
    }

    $scope.getLogin = function () {
        //if ($scope.$valid === true) {
        authService.post({
                user: $scope.name,
                pw: $scope.pw,
                cache: Date.now()
            },
            function (data) {
                WorkshopDataService.user = data;
                //$location.path('data');
                $window.location.reload();
            },
            function (response) {
                $scope.errors = response.data;
            });
        //}

    };

    $scope.register = function(){
        var newUser = registrationService.post({
            username: $scope.name,
            email: $scope.name,
            password: $scope.pw
        }).$promise;

        newUser.then(function(){
            $scope.getLogin();
        });

        newUser.catch(function(response){
            $scope.errors = response.data;
        })
    };
    checkAuthentication();
}]);
