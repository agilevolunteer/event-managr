angular.module('eventManagrCart')
  .service('cartService', ['$resource', 'APP_SETTINGS', function ($resource, settings) {
    var cartResource = $resource(settings.api + 'eventmanagr/engagement', {}, {
      get: {
        method: 'GET',
        headers: {
          "X-WP-NONCE": settings.nonce
        }
      }
    });
    var invitationResource = $resource(settings.api + 'eventmanagr/engagement/tender', {}, {
      post: {
        method: 'POST',
        headers: {
          "X-WP-NONCE": settings.nonce
        }
      }
    });
    var Service = {
      getCart: cartResource.get,
      tenderEngagement: invitationResource.post
    };

    return Service;
  }]);


angular.module('eventManagrCart')
  .controller('cartController', ['$scope', 'cartService', ($scope, cartService) => {
    $scope.mainDepartment = () => {
      if ($scope.cart.mainDepartment)
        return $scope.cart.mainDepartment;
      else
        return {};
    };
    $scope.editUser = false;

    $scope.tender = (cartid) => {
      let tenderData = {
        cartId: $scope.cart.cartId
      };

      cartService.tenderEngagement(tenderData).$promise
      .then((data) => {
        $scope.cart.invitation = data.invitation
      })
    }

    function loadCart(){
      cartService.getCart().$promise
        .then(function(data){
          $scope.cart = data;
        },function(){
          $scope.cart = {};
        });
    }
    loadCart();
}]);
