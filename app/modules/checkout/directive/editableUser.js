'use strict';
angular.module('eventManagrCart')
.directive('editableUser', ['UserDataService', 'APP_SETTINGS', 'NotifyService', function(UserDataService, APP_SETTINGS, NotifyService) {
    return {
        scope: {
          me: '=user'
        },
        link: (scope, element) => {
          let writeButton = () => {
            if (scope.editable){
              scope.buttontext = 'Beenden';
            } else {
              scope.buttontext = 'Bearbeiten';
            }
          };
          scope.editable = false;
          scope.toggleEdit  = () => {
            scope.editable = !scope.editable;
            console.log("editieren");
            NotifyService.warn("editieren");
            writeButton();
          };
          scope.saveUser = () => {
            let request = {

              ID: scope.me.id,
              id: scope.me.id,
              username: scope.me.email,
              email: scope.me.email,
              first_name: scope.me.firstName,
              last_name: scope.me.lastName,
              zip: scope.me.zip
            };
            UserDataService.put(request,
            (data) => {
              NotifyService.success(data);
              scope.me.id = data.ID;
              scope.me.firstName = data.first_name;
              scope.me.lastName = data.last_name;
              scope.me.email = data.email;
              scope.me.zip = data.meta.zip;

            },
            (error) => {
              console.log(error);
              NotifyService.failed(error.message);

            });
          };
           writeButton();
        },
        templateUrl: APP_SETTINGS.templateRoot + 'views/user/editable.html'
    }
}]);
