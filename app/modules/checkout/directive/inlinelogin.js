angular.module('eventManagrCart')
    .factory('authService', ['$resource', 'APP_SETTINGS', function($resource, settings) {
        return $resource(settings.api + 'eventmanagr/people/me', {}, {
            get: {
                method: "GET",
                headers: {
                    "X-WP-NONCE": settings.nonce
                }
            },
            post: {
                method: "POST"
            }
        });
    }])
    .factory('registrationService', ['$resource', 'APP_SETTINGS', function($resource, settings) {
        return $resource(settings.api + 'eventmanagr/people/new', {}, {
            post: {
                method: "POST"
            }
        });
    }])
    .controller('authenticationController', ['$scope', 'authService', 'registrationService', '$window', 'NotifyService', function($scope, authService, registrationService, $window, NotifyService) {
        $scope.name = "";
        $scope.pw = "";
        $scope.errors = [];
        $scope.isAuthenticationRequired = false;

        $scope.getLogin = function() {
            console.log($scope);
            //if ($scope.$valid === true) {
            authService.post({
                    user: $scope.name,
                    pw: $scope.pw,
                    cache: Date.now()
                },
                function(data) {
                    console.log(data);
                    //$window.location.reload();
                },
                function(response) {
                    $scope.errors = response.data;
                });
        };

        $scope.register = function() {
            var newUser = registrationService.post({
                username: $scope.name,
                email: $scope.name,
                password: $scope.pw
            }).$promise;

            newUser.then(function(data) {
                $window.location.reload();
            });

            newUser.catch(function(response) {
                $scope.errors = response.data;
                NotifyService.failed('Deine Anmeldung ist fehlgeschlagen');
            })
        };


    }])
    .directive('inlineLogin', function(APP_SETTINGS) {
        return {
            templateUrl: APP_SETTINGS.templateRoot + 'views/auth/login.html'
        }
    });
