'use strict';

 var settings = {};
 if (window.AGV_WORKSHOP_SETTINGS !== undefined) {
   settings = AGV_WORKSHOP_SETTINGS;
   if (settings.api[settings.api.length - 1] !== '/') {
     settings.api = settings.api + '/';
   }
 }
angular.module('agv.common', ['ngRoute']);
angular.module('agv.common').constant('APP_SETTINGS', settings);
