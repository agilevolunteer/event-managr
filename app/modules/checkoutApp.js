"use strict";

angular.module('eventManagrCart', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch',
  'pascalprecht.translate',
  'agv.common',
  'ngToast'
]);

var Cart = angular.module('eventManagrCart');
var settings = {};
if (window.AGV_WORKSHOP_SETTINGS !== undefined) {
  settings = AGV_WORKSHOP_SETTINGS;
  if (settings.api[settings.api.length - 1] !== '/') {
    settings.api = settings.api + '/';
  }
}

Cart.constant('APP_SETTINGS', settings);
Cart.config(function($routeProvider, $httpProvider, APP_SETTINGS){
  //$httpProvider.defaults.withCredentials = true;
  $routeProvider
    .when('/', {
      templateUrl: APP_SETTINGS.templateRoot + 'views/cart.html',
      controller: 'cartController'
    })
    .otherwise({redirectTo: '/'});
});
