/**
 * @ngdoc service
 * @name workshopRegApp.WorkshopDataService
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $replace_me
 *
 * */


angular.module('workshopRegApp')
    .service('WorkshopDataService', ['$resource', 'APP_SETTINGS', function($resource, settings){

    var Service = {
        user: undefined,
        taxonomies: $resource(settings.api + 'taxonomies/:tax/terms'),
        workshops: $resource(settings.api + 'posts',{
        }, {
            query: {
                method: "GET",
                headers: {
                    "X-WP-NONCE": settings.nonce
                },
                cache: false,
                isArray: true
            },
            save: {
                method: "POST",
                headers: {
                    "X-WP-NONCE": settings.nonce
                },
                cache: false
            }
        })
    };

    return Service;
}]);

