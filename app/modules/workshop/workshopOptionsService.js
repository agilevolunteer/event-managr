/**
 * @ngdoc service
 * @name workshopOptionsApp.workshopOptionsService
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $replace_me
 *
 * */


angular.module('workshopOptionsApp')
    .service('workshopOptionsService', ['$resource', 'APP_SETTINGS', function ($resource, settings) {

        var workshopOptionsResource = $resource(settings.api + 'eventmanagr/options/workshops');
        var sectionsResource = $resource(settings.api + 'eventmanagr/sections');

        var Service = {
            getOptions: workshopOptionsResource.get,
            saveOptions: workshopOptionsResource.save,
            getSections: sectionsResource.query
        };

        return Service;
    }]);

