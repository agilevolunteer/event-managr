/**
 * @ngdoc controller
 * @name workshopOptions.controller:workshopOptionsCtrl
 *
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $scope
 * */


angular.module('workshopOptionsApp')
    .controller('workshopOptionsCtrl', ['$scope', 'workshopOptionsService', function ($scope, service) {

        $scope.options = {};
        $scope.setOptions = function (data) {
            $scope.options = data;
        };
        $scope.saveOptions = function(){
            service.saveOptions($scope.options);
        };

        $scope.sections = [];
        $scope.setSections = function (data) {
            $scope.sections = data;
        };

        function init() {
            service.getOptions({}, $scope.setOptions);
            service.getSections({}, $scope.setSections);
        }

        init();
    }]);
