'use strict';

/**
 * Angular Module
 * @ngdoc overview
 * @name workshopRegApp
 * @description
 * checkoutApp
 *
 * Main module of the application.
 */
angular
    .module('workshopRegApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'pascalprecht.translate',
        'agv.common',
        'ngToast'

    ]);

/**
 * workshopRegApp
 * @name workshopRegApp
 * @description
 * create var for module workshopRegApp
 */
var workshopRegApp = angular.module('workshopRegApp');
var settings = {};
if (window.AGV_WORKSHOP_SETTINGS !== undefined) {
    settings = AGV_WORKSHOP_SETTINGS;
    if (settings.api[settings.api.length - 1] !== '/') {
        settings.api = settings.api + '/';
    }
}
//console.log(settings);

workshopRegApp.constant('APP_SETTINGS', settings);
/**
 * Routing and Cookies
 * @description
 * Config for Routing and Cookies
 * */
workshopRegApp.config(function ($routeProvider, $httpProvider, APP_SETTINGS) {
    $httpProvider.defaults.withCredentials = true;
    $routeProvider
        .when('/', {
            templateUrl: APP_SETTINGS.templateRoot + 'views/loginView.html',
            controller: 'authController'
        })
        .when('/data', {
            templateUrl: APP_SETTINGS.templateRoot + 'views/userDataView.html',
            controller: 'workshopDataCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
});
