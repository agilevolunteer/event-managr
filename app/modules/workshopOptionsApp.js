var optionsApp = angular.module('workshopOptionsApp', [
    'ng',
    'ngTouch',
    'ngResource'
]);

optionsApp.constant('APP_SETTINGS', window.AGV_WORKSHOP_SETTINGS);
