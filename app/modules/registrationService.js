workshopRegApp.factory('registrationService', ['$resource', 'APP_SETTINGS', function ($resource, settings) {
    return $resource(settings.api + 'eventmanagr/people/new', {}, {
        post: {
            method: "POST"
        }
    });
}]);
