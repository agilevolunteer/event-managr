<div class="wrap" ng-app="workshopOptionsApp" ng-controller="workshopOptionsCtrl">
    <h2>Einstellungen für Workshops</h2>


    <label><b>3x7job Bereich für Anmeldungen:</b></label>

    <div><i>{{options.AgvSection4Workshop.text}}</i></div>
    <ul>

        <li ng-repeat="section in sections"
            ng-hide="section.id === options.AgvSection4Workshop.id"
            ng-click="options.AgvSection4Workshop = section">{{section.text}}</li>
    </ul>
    <button class="button button-primary" ng-click="saveOptions()">Speichern</button>
</div>

