
<div ng-app="workshopRegApp" class="event-managr">
    <toast></toast>
    <div data-ng-view=""></div>
</div>

<!-- build:js(.) scripts/vendor.js -->
<!-- bower:js -->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--bower_components/jquery/dist/jquery.js"></script>-->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--bower_components/angular/angular.js"></script>-->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--bower_components/bootstrap/dist/js/bootstrap.js"></script>-->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--bower_components/angular-resource/angular-resource.js"></script>-->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--bower_components/angular-cookies/angular-cookies.js"></script>-->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--bower_components/angular-sanitize/angular-sanitize.js"></script>-->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--bower_components/angular-animate/angular-animate.js"></script>-->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--bower_components/angular-touch/angular-touch.js"></script>-->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--bower_components/angular-route/angular-route.js"></script>-->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--bower_components/angular-translate/angular-translate.js"></script>-->
<!-- endbower -->
<!-- endbuild -->

<!-- build:js({.tmp,app}) scripts/scripts.js -->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--modules/workshopRegApp.js"></script>-->
<!--<script src="--><?php //echo AGV_WORKSHOP_URL; ?><!--modules/loginController.js"></script>-->
<!-- endbuild -->

<!--<script src="//localhost:35729/livereload.js"></script>-->
