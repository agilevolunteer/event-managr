<?php

namespace AGV\eventManagr\test;


class EngagementCartTest extends \WP_UnitTestCase {
	private $queryVars;
	public function setUp(){
		global $wp;
		parent::setUp();
		\AGV\eventManagr\Checkout\EngagementCart::init();
		$this->queryVars = $wp->public_query_vars;
	}

	function testTemplateRedirect(){
		$this->assertEquals(10, has_action('template_redirect', array(\AGV\eventManagr\Checkout\EngagementCart::class, 'apply')));
	}

	function testQueryVarAdded(){
		global $wp;
		\AGV\eventManagr\Checkout\EngagementCart::addQueryVar();
		$this->assertTrue(in_array('engagement', $wp->public_query_vars));
	}
}