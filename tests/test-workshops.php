<?php

class WorkshopTest extends WP_UnitTestCase {
	private $taxonomies;
	private $postTypes;

	public function setUp() {
		parent::setUp();
		AgvRegisterWorkshopPosttype();
		AgvRegisterPlaceAndTimeTaxonomy();
		AgvRegisterWorkshopCapabilities();
		$this->taxonomies = get_object_taxonomies('workshops');
		$this->postTypes = get_post_types();
	}

	function testWorkshopPostTypeIsAvalable() {
		$this->assertTrue( array_key_exists('workshops', $this->postTypes) );
	}

	function testWorkshopHasTime() {
		$this->assertTrue( in_array("times", $this->taxonomies));
	}
}