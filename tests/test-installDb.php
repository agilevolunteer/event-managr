<?php

namespace AGV\eventManagr\test;


class InsallDbTest extends \WP_UnitTestCase {
	private $queryVars;
	public function setUp(){
		global $wp;
		parent::setUp();
		\AGV\eventManagr\database\AgvInstaller::init();

		$this->queryVars = $wp->public_query_vars;
	}

	/*
	function testTemplateRedirect(){
		$this->assertEquals(10, has_action('template_redirect', array(\AGV\eventManagr\Checkout\EngagementCart::class, 'apply')));
	}
	*/

	function testQueryVarAdded(){
		\AGV\eventManagr\database\AgvInstaller::addQueryVar();
		global $wp;
		$this->assertTrue(in_array('update-events', $wp->public_query_vars));
	}
}