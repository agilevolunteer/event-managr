<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcel
 * Date: 04.09.15
 * Time: 22:16
 */

namespace AGV\eventManagr\test;

use AGV\eventManagr;

class DepartmentTest extends \WP_UnitTestCase{
	private $postTypes;
	private $department;

	public function setUp(){
		parent::setUp();
		eventManagr\DepartmentPostType::init();
		do_action("init");

		$this->postTypes = get_post_types();
		$this->department = get_post_type_object("department");
	}

	function testDepartmentIsAvailable(){
		$this->assertTrue( in_array('department', $this->postTypes) );
	}

	function testDepartmentIsPublic(){
		//print_r($this->department);
		$this->assertTrue($this->department->public);
		$this->assertTrue($this->department->show_ui);
		$this->assertTrue($this->department->show_in_menu);
		$this->assertTrue($this->department->show_in_nav_menus);
	}

	function testDepartmentLabels(){
		$this->assertEquals("Bereiche", $this->department->label);
	}

	function testDepartmentMenuIntegration(){
		$this->assertTrue($this->department->show_in_nav_menus);
	}

}