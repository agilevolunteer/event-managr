<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcel
 * Date: 12.09.15
 * Time: 21:39
 */

namespace AGV\eventManagr\Checkout;

class EngagementCart {
	private static $queryVar = 'engagement';

	public static function init(){
		add_action('init', array(__CLASS__, 'addQueryVar'));
		add_action('template_redirect', array(__CLASS__, 'apply'));

	}

	public static function apply(){
		if (get_query_var( self::$queryVar )){
			//echo "Checkout";/
			do_action("agv_workshops_enqueue_scripts");
			include(AGV_WORKSHOP_DIR.'templates/checkout.php');
			die();
		}
	}

	public static function addQueryVar(){
		add_rewrite_rule( '^'. self::$queryVar .'/?$','index.php?'. self::$queryVar .'=/','top' );
		global $wp;
		$wp->add_query_var( self::$queryVar );
	}
}