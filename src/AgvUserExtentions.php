<?php

/**
 * Created by IntelliJ IDEA.
 * User: agilevolunteer
 * Date: 03.01.2015
 * Time: 20:46
 */
namespace AGV\eventManagr;

class AgvUserExtentions
{
    function __construct()
    {
        add_action('json_insert_user', array($this, 'insertExtentedUser'), 10, 3);
        add_action('json_prepare_user', array($this, 'prepareExtendedUser'), 10, 3);
        add_action('json_query_vars', array($this, 'extentQueryVars'), 10, 1);
    }

    function extentQueryVars($vars){
        array_push($vars, 'post_status');

        return $vars;
    }

    function insertExtentedUser($user, $data, $update)
    {
        if($update == true){
            update_user_meta($user->ID, 'zip', $data['zip']);
            update_user_meta($user->ID, 'phone', $data['phone']);
            update_user_meta($user->ID, 'job', $data['job']);
            update_user_meta($user->ID, 'church', $data['church']);
            update_user_meta($user->ID, 'churchContact', $data['churchContact']);
            update_user_meta($user->ID, 'experience', $data['experience']);
            update_user_meta($user->ID, 'community_optin', $data['community_optin']);

        } else {
            add_user_meta($user->ID, 'zip', $data['zip']);
            add_user_meta($user->ID, 'phone', $data['phone']);
            add_user_meta($user->ID, 'job', $data['job']);
            add_user_meta($user->ID, 'church', $data['church']);
            add_user_meta($user->ID, 'churchContact', $data['churchContact']);
            add_user_meta($user->ID, 'experience', $data['experience']);
            add_user_meta($user->ID, 'community_optin', $data['community_optin']);
        }


    }

    function prepareExtendedUser($user_fields, $user, $context ){
        $user_fields["meta"]["zip"] = $user->zip;
        $user_fields["meta"]["phone"] = $user->phone;
        $user_fields["meta"]["job"] = $user->job;
        $user_fields["meta"]["church"] = $user->church;
        $user_fields["meta"]["churchContact"] = $user->churchContact;
        $user_fields["meta"]["experience"] = $user->experience;
        $user_fields["meta"]["community_optin"] = $user->community_optin;

        return $user_fields;
    }

}
