<?php
namespace AGV\eventManagr;

class Agv3x7jobConnector{
    public function __construct(){
        add_action('AgvApiGetSections', array($this, 'getSections'), 10, 2);
        add_action('json_insert_user', array($this, 'saveWorkshopRegistration'), 10, 3);
        add_action('json_prepare_user', array($this, 'prepareExtendedUser'), 10, 3);
    }



    function saveWorkshopRegistration($user, $data, $update){
        //echo $data['volunteerContext'];
        if (FsmaUserHasAnmeldung($user->ID, '', true)){
            return;
        }

        if(isset($data['volunteerContext']) && $data['volunteerContext'] == 'workshop'){
            $workshopSection = get_option('AgvSection4Workshop');
            FsmaInsertLightAnmeldung($workshopSection, $user->ID);
        }
    }


    function getSections($sections, $id = -1){
        $prod = "Bereiche4DropDown.sql";
        global $table_prefix;
        $where = "1=1";
        if ($id != -1  && $id != ''){
            $where = 'BID = '.$id;

        }
        $values  = new \jfbremenTemplate(FSMASQL.$prod);
        $values = $values->DoMultipleQuery(true, array("__PREFIX__" => $table_prefix, "__WHERE__" => $where), OBJECT);

        $retVal = null;
        if($id != -1){
            return $values[0][0];
        }

        return array_merge($sections, $values[0]);
    }

    function prepareExtendedUser($user_fields, $user, $context ){
        $user_fields["eventManagr"]["hasWorkshopRegistration"] = FsmaUserHasAnmeldung($user->ID, '', true);

        return $user_fields;
    }
}
