<?php
namespace AGV\eventManagr;

use AGV\eventManagr\api;
use \WP_JSON_Server;
//include_once( AGV_DEP_WP_API_PATH . '/lib/class-wp-json-users.php' );

class AgvWorkshopHandler {

    private $apiAuthController;
    private $apiOptionsController;
    private $x7Connector;

    function __construct(){
        add_action('wp_json_server_before_serve', array($this, 'initApi'));

    }

    function  initApi(){
        $this->apiAuthController = new api\AgvApiAuthController(new WP_JSON_Server());
        add_filter('json_endpoints', array($this->apiAuthController, 'register_routes'));

        $this->x7Connecctor = new Agv3x7jobConnector();

        $this->apiOptionsController = new api\AgvApiOptionsController();
        add_filter('json_endpoints', array($this->apiOptionsController, 'register_routes'));

    }

	function replaceContent( $path ) {
		//$content = file_get_contents($dir.'app/workshopRegistrationApp.php');
		ob_start();
		include( $path );
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

    function getShortCodeReplacement(){
	    do_action('agv_workshops_enqueue_scripts');

	    return $this->replaceContent(AGV_WORKSHOP_DIR . 'app/workshopRegistrationApp.php');
    }
//
    function getOptionsContent(){
        include_once(AGV_WORKSHOP_DIR . 'app/workshopOptionsApp.php');
        //return $this->replaceContent(AGV_WORKSHOP_DIR . 'app/workshopOptionsApp.php');

    }

    function getScheduleContent(){
        include_once(AGV_WORKSHOP_DIR . 'app/workshopScheduleApp.php');
        //return $this->replaceContent(AGV_WORKSHOP_DIR . 'app/workshopOptionsApp.php');

    }
}
