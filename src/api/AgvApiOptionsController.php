<?php
namespace AGV\eventManagr\api;
use \WP_JSON_Server;
//include_once( AGV_DEP_WP_API_PATH . '/lib/class-wp-json-server.php' );

/**
 * Created by IntelliJ IDEA.
 * User: mhenning
 * Date: 16.01.2015
 * Time: 21:59
 */
class AgvApiOptionsController
{
    /**
     * @param $routes
     * @return array
     */
    public function register_routes($routes)
    {
        $optionsRoutes = array(
            '/eventmanagr/options/workshops' => array(
                array(
                    array($this, 'saveWorkshopOptions'), \WP_JSON_Server::CREATABLE | \WP_JSON_Server::ACCEPT_JSON
                ),
                array(
                    array($this, 'getWorkshopOptions'), \WP_JSON_Server::READABLE
                )

            ),
            '/eventmanagr/sections' => array(
                array(
                    array($this, 'getSections')

                )
            )
        );

        return array_merge($routes, $optionsRoutes);
    }

    /**
     * @return array
     */
    function getWorkshopOptions(){

        $options = array();
        $section4Workshop = -1;
        $section4Workshop = apply_filters('AgvApiGetSections', array(), get_option('AgvSection4Workshop'));

        $options['AgvSection4Workshop'] = $section4Workshop;
        return $options;

    }

    function saveWorkshopOptions($data){
        return update_option('AgvSection4Workshop', $data["AgvSection4Workshop"]["id"]);
    }

    /**
     * @return mixed|void
     */
    function getSections(){
        $sections = array();
        return apply_filters('AgvApiGetSections', $sections);;

    }
}
