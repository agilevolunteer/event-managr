<?php
namespace AGV\eventManagr\api\dto;
/**
 * Created by IntelliJ IDEA.
 * User: marcel
 * Date: 20.11.15
 * Time: 11:47
 */
class CartDto {
	public $cartId;
	public $lineItems;
	public $user;
	public $invitation;

	public function __construct($lineItems, $userId, $cartId){
		$this->cartId = $cartId;
		$this->lineItems = $lineItems;
		if ($userId != 0) {
			$this->user = new UserDto($userId);
		}
	}
}

class UserDto {
	public $firstName;
	public $lastName;
	public $email;
	public $id;
	public $zip;
	public $raw;

	public function __construct($id) {
		$user = get_userdata($id);
		$this->firstName = $user->first_name;
		$this->lastName = $user->last_name;
		$this->id = $user->ID;
		$this->email = $user->user_email;
		$this->zip = get_user_meta($id, 'zip', true);


		//$this->raw = $user;
	}

}
