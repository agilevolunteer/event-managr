<?php
namespace AGV\eventManagr\api;
use \WP_JSON_Users;
use \WP_JSON_Server;
//
//function AgvApiAuthInit()
//{
//    global $agvApiAuthCtrl;
//    // Users
//    $wp_json_users = new WP_JSON_Users( new \WP_JSON_Server() );
//
//    $agvApiAuthCtrl = new AgvApiAuthController(new \WP_JSON_Server() );
//    add_filter('json_endpoints', array($agvApiAuthCtrl, 'register_routes'));
//}
//
//add_action('\WP_JSON_Server_before_serve', 'AgvApiAuthInit');

//include_once( AGV_DEP_WP_API_PATH . 'lib/class-wp-json-users.php' );
class AgvApiAuthController extends \WP_JSON_Users
{
    protected $userCtrl;

    public function register_routes($routes)
    {
        $authRoutes = array(
            '/eventmanagr/people/me' => array(
                array(
                    array( $this, 'getCurrentUser' ), \WP_JSON_Server::READABLE
                ),
                array(
                    array($this, 'checkLogin'), \WP_JSON_Server::CREATABLE | \WP_JSON_Server::ACCEPT_JSON)
                ),
            '/eventmanagr/people/new' => array(
                array(
                    array( $this, 'registerNewUser'), \WP_JSON_Server::CREATABLE | \WP_JSON_Server::ACCEPT_JSON
                )
            )
        );

        return array_merge( $routes, $authRoutes );
    }



    public function getCurrentUser(){

        $id = get_current_user_id();

        $user = get_userdata( $id );

        if ( empty( $user->ID ) ) {
            return new \WP_Error( 'json_user_invalid_id', __( 'Invalid user ID.' ), array( 'status' => 400 ) );
        }

        return $this->prepare_user( $user );

    }

    public function checkLogin($data)
    {
        $user = wp_authenticate($data['user'], $data['pw']);

        if (is_wp_error($user)) {
            return $user;
        }

        wp_set_auth_cookie($user->ID);
        return $this->prepare_user( $user );
    }

    public function registerNewUser($data)
    {
        $user = new \stdClass;


        if ( !empty( $data['ID'] ) ) {
            return new \WP_Error('json_user_exists', 'Ein bereits existierender Benutzer kann nicht neu registiert werden', array( 'status' => 406 ));
        }

        $required = array( 'username', 'password', 'email' );

        foreach ( $required as $arg ) {
            if ( empty( $data[ $arg ] ) ) {
                return new \WP_Error( 'json_missing_callback_param', sprintf( __( 'Missing parameter %s' ), $arg ), array( 'status' => 400 ) );
            }
        }

        // Basic authentication details
        if ( isset( $data['username'] ) ) {
            $user->user_login = $data['username'];
        }

        if ( isset( $data['password'] ) ) {
            $user->user_pass = $data['password'];
        }

        // Names
        if ( isset( $data['name'] ) ) {
            $user->display_name = $data['name'];
        }

        if ( isset( $data['first_name'] ) ) {
            $user->first_name = $data['first_name'];
        }

        if ( isset( $data['last_name'] ) ) {
            $user->last_name = $data['last_name'];
        }

        // Email
        if ( ! empty( $data['email'] ) ) {
            $user->user_email = $data['email'];
        }

        // Pre-flight check
		$user = apply_filters( 'json_pre_insert_user', $user, $data );

		if ( is_wp_error( $user ) ) {
            return $user;
        }

        $user_id = wp_insert_user($user);

        if ( is_wp_error( $user_id ) ) {
            return $user_id;
        }

        $user->ID = $user_id;

        do_action( 'json_insert_user', $user, $data, false );

        return $user_id;
    }
}
