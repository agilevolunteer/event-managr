<?php
namespace AGV\eventManagr\api;
use AGV\eventManagr\api\dto\CartDto;
use AGV\eventManagr\database\CartItemsTable;
use AGV\eventManagr\database\CartTable;
use AGV\eventManagr\database\EngagementTable;
use \WP_JSON_Server;
//include_once( AGV_DEP_WP_API_PATH . '/lib/class-wp-json-server.php' );

/**
 * Created by IntelliJ IDEA.
 * User: mhenning
 * Date: 16.01.2015
 * Time: 21:59
 */
class CartController
{
	protected $cartTable;
	protected $cartItemsTable;
	protected $engagementTable;

	const AGV_EM_VID = "agv-em-vid";

	function __construct() {
		$this->cartTable = new CartTable();
		$this->cartItemsTable = new CartItemsTable();
		$this->engagementTable = new EngagementTable();
		add_action('wp_json_server_before_serve', array($this, 'initApi'));
	}

	function initApi(){
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

		add_action('json_endpoints', array($this, 'register_routes'));
	}

	/**
	 * @param $routes
	 * @return array
	 */
	public function register_routes($routes)
	{
		$cartRoutes = array(
			'/eventmanagr/engagement' => array(
				array(
					array($this, 'addToCart'), \WP_JSON_Server::CREATABLE | \WP_JSON_Server::ACCEPT_JSON
				),
				array(
					array($this, 'getCart'), \WP_JSON_Server::READABLE
				)
			),
			'/eventmanagr/engagement/tender' => array(
				array(
					array($this, 'tenderEngagement'), \WP_JSON_Server::CREATABLE | \WP_JSON_Server::ACCEPT_JSON
				)
			)
		);

		return array_merge($routes, $cartRoutes);
	}

	function getCartId(){
		return $_COOKIE[ self::AGV_EM_VID ];
	}

	function getInvitationForCart($cartId){
		return $this->engagementTable->get_by(array("cartId" => $cartId))[0]->id;
	}

	public function tenderEngagement($data) {
		$cartId = $data["cartId"];
		$invitation = uniqid("", true);
		$insertData = array(
			"id" => $invitation,
			"cartId" => $cartId,
			"userId" => get_current_user_id()
		);

		$this->engagementTable->insert($insertData);
		return array("invitation" => $this->getInvitationForCart($cartId));
	}

	/**
	 * @return array
	 */
	public function addToCart($bid) {

		setcookie("agv-em-bid", $bid, time() + 60*60*24*30, '/');
		$vid = null;
		if (!$_COOKIE[ self::AGV_EM_VID ]){
			$vid = uniqid("", true);
			setcookie( self::AGV_EM_VID, $vid, time() + 60*60*24*30, '/');
		} else {
			$vid = $this->getCartId();
		}


		$this->cartTable->insert(array(
			"id" => $vid
		));

		$this->cartItemsTable->insert(array(
			"id" => $vid,
			"departmentId" => $bid
		));


		header("Location: ".home_url( '/engagement' ));
		exit;
	}

	public function getCart(){
		global $wpdb;
		$vid = $this->getCartId();

		$sql = "
			SELECT p.*
			FROM ". $this->cartItemsTable ." ci
			INNER JOIN $wpdb->posts p
			ON ci.departmentId = p.ID
			WHERE ci.id = '$vid'
		";

		//return $sql;
		$items = $wpdb->get_results($sql);
		$cartDto = new CartDto($items, get_current_user_id(), $vid);
		$cartDto->invitation = $this->getInvitationForCart($vid);
		return $cartDto;
	}
}
