<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcel
 * Date: 10.10.15
 * Time: 16:14
 */

namespace AGV\eventManagr\demo;
use \WP_JSON_Server;

class AgvDepartmentsDemo {

	function __construct() {
		add_action('wp_json_server_before_serve', array($this, 'initApi'));
	}

	function initApi(){
		add_action('json_endpoints', array($this, 'register_routes'));
	}

	public function register_routes($routes){
		$demoRoutes = array(
			'/eventmanagr/demo/departments' => array(
				array(
					array($this, 'generateDemoDepartments'), \WP_JSON_Server::CREATABLE
				)
			)
		);

		return array_merge($routes, $demoRoutes );
	}

	function generateDemoDepartments(){


		$samplePost1 = array(
			"post_content" => "Lorem Ipsum 123. Great Content is great. All hail to the content",
			"post_title" => "Demo Dempartment 1",
			"post_status" => "publish",
			"post_author" => 1,
			"post_type" => "department"
		);

		$existingPost = get_page_by_title($samplePost1["post_title"], OBJECT, "department");

		$samplePost1["ID"] = $existingPost->ID;

		return wp_insert_post($samplePost1);
	}
}