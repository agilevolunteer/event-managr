<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcel
 * Date: 03.09.15
 * Time: 18:36
 */

namespace AGV\eventManagr;
class DepartmentPostType {

	public static function init(){
		add_action("init", array(__CLASS__, "registerPostType"));
	}

	public static function registerPostType(){
		$text = __("Bereiche", "wp-event-managr");

		$labels = array(
			'name' => __('Bereiche', 'wp-event-managr'),
			'singular_name' => __("Bereich", "wp-event-managr"),
			'menu_name' => __("Bereiche", "wp-event-managr"),
			'parent_item_colon' => __("Übergeordneter Bereich", "wp-event-managr"),
			'all_items' => __("Alle Bereiche", "wp-event-managr"),
			'view_item' => __("Bereiche anzeigen", "wp-event-managr"),
			'add_new_item' => __("Neuen Bereich anlegen", "wp-event-managr"),
			'add_new' => __("Neuer Bereich", "wp-event-managr"),
			'edit_item' => __("Bereich bearbeiten", "wp-event-managr"),
			'update_item' => __("Bereich aktualisieren", "wp-event-managr"),
			'search_items' => __("Bereich suchen", "wp-event-managr")

		);

		$args = array(
			"public" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"labels" => $labels,
			"supports" => array('title', 'editor', 'author', 'custom-fields'),
			"has_archive" => true


		);
		register_post_type("department", $args);
	}
}