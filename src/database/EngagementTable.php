<?php
namespace AGV\eventManagr\database;

class EngagementTable extends AgvBaseTable {
  function __construct(){
    global $wpdb;
    parent::__construct($wpdb->prefix."agv_engagements");
  }
}
