<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcel
 * Date: 15.11.15
 * Time: 22:41
 */

namespace AGV\eventManagr\database;


class CartItemsTable extends AgvBaseTable {
	function __construct(){
		global $wpdb;
		parent::__construct($wpdb->prefix."agv_cartItems");
	}
}