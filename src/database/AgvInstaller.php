<?php
namespace AGV\eventManagr\database;


/**
 * Created by IntelliJ IDEA.
 * User: marcel
 * Date: 15.11.15
 * Time: 20:51
 */
class AgvInstaller {
	const UPDATE_EVENTS = "update-events";

	const CARTS = 'agv_carts';
	const CARTITEMS = "agv_cartItems";
	const ENGAGEMENTS = "agv_engagements";

	public static function init(){
		register_activation_hook( __CLASS__, 'createTables' );

		add_action('template_redirect', array(__CLASS__, 'apply'));
		add_action('init', array(__CLASS__, 'addQueryVar'));
	}

	public static function apply(){
		if ($updateHash = get_query_var( self::UPDATE_EVENTS )) {
			if ( $updateHash == "Lr5o8s03s8u5iiv0" ) {
				echo "Installing tables";
				AgvInstaller::createTables();
			} else {
				echo "entered wrong hast, exiting";
			}
			die();
		}
	}

	public static function addQueryVar() {
		global $wp;
		$wp->add_query_var( self::UPDATE_EVENTS );
	}

	public static function createTables(){
		global $wpdb;
		echo "create tables, hash:". uniqid();
		$wpdb->show_errors();
		// current blog table prefix
		$cartsTable = $wpdb->prefix . self::CARTS;
		$cartsItemsTable = $wpdb->prefix . self::CARTITEMS;
		$engagementsTable = $wpdb->prefix . self::ENGAGEMENTS;

		$createCarts = "CREATE TABLE $cartsTable (
	      id VARCHAR(24) NOT NULL,
	      UNIQUE KEY id (id)
    );";

		$createCartItems = "CREATE TABLE $cartsItemsTable (
	      id VARCHAR(24) NOT NULL,
	      departmentId INT NOT NULL,
	      PRIMARY KEY(id, departmentId)
    );";

		$createEngagements = "CREATE TABLE $engagementsTable (
				id VARCHAR(24) NOT NULL,
				cartId VARCHAR(24) NOT NULL,
				userId INT NOT NULL,
				PRIMARY KEY(id)
		);";

		$uniqueIndexCartItems = "CREATE UNIQUE INDEX unique_cart_items ON $cartsItemsTable (id, departmentId);";
		$uniqueIndexEngagement = "CREATE UNIQUE INDEX unique_engagement_user ON $engagementsTable (cartId, userId);";
		//echo $uniqueIndexEngagement;
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $createCarts );
		dbDelta( $createCartItems );
		dbDelta( $createEngagements );
		$wpdb->query($uniqueIndexCartItems);
		$wpdb->query($uniqueIndexEngagement);
		$wpdb->hide_errors();
	}
}
