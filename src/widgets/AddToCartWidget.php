<?php
namespace AGV\eventManagr\widgets;
/**
 * Created by IntelliJ IDEA.
 * User: marcel
 * Date: 15.11.15
 * Time: 23:06
 */
class AddToCartWidget {
	static function render($args) {
		global $post;
		if(!is_archive() && $post->post_type == "department"){
			do_action("agv_workshops_enqueue_scripts");
			echo $args['before_widget'];
//			echo $args['before_title'] . 'My Unique Widget' .  $args['after_title'];
			echo $args['after_widget'];
			// print some HTML for the widget to display here
			?>
			<form class="event-managr" action="<?php echo esc_url( home_url( '/wp-json/eventmanagr/engagement' ) ); ?>" method="post">
				<input type="hidden" value="<?php echo $post->ID; ?>" name="bid"/>
				<button class="agv-button agv-button--primary">Mitarbeiten</button>
			</form>
			<?php
		}

	}

	static function init() {
		wp_register_sidebar_widget(
			'agv_em_add_to_cart',        // your unique widget id
			'AddToCart',          // widget name
			array(__CLASS__, "render"),  // callback function
			array(                  // options
				'description' => 'Description of what your widget does'
			)
		);
	}
}