<?php
namespace AGV\eventManagr;
/*
Plugin Name: Event Managr
Version: 0.1.0
Description: Helps to administer and organize a Event with volunteers, workshops and room services
Author: agilevolunteer
Author URI: http://agilevolunteer.com/uber-mich/
Plugin URI: http://agilevolunteer.com/uber-mich/
Text Domain: wp-event-managr
Domain Path: /languages
*/


spl_autoload_register(__NAMESPACE__ . '\\agvAutoload');
function agvAutoload($cls)
{
  $cls = ltrim($cls, '\\');
  if(strpos($cls, __NAMESPACE__) !== 0)
    return;

  $cls = str_replace(__NAMESPACE__, '', $cls);

  $path = AGV_WORKSHOP_DIR . 'src' .
          str_replace('\\', DIRECTORY_SEPARATOR, $cls) . '.php';
  require_once($path);
}



define('AGV_WORKSHOP_DIR', plugin_dir_path(__FILE__));
define('AGV_WORKSHOP_URL', plugins_url('', __FILE__) . '/');
define('AGV_DEP_WP_API_PATH', WP_PLUGIN_DIR . '/json-rest-api/');
define('AGV_DEVELOP', true);
include_once(ABSPATH . 'wp-admin/includes/plugin.php');
include(AGV_WORKSHOP_DIR . 'src/WorkshopsPosttype.php');
/*
include(AGV_WORKSHOP_DIR . 'src/api/AgvApiAuthController.php');
include(AGV_WORKSHOP_DIR . 'src/api/AgvApiOptionsController.php');
include(AGV_WORKSHOP_DIR . 'src/AgvWorkshopHandler.php');
include(AGV_WORKSHOP_DIR . 'src/AgvUserExtentions.php');
include(AGV_WORKSHOP_DIR . 'src/Agv3x7jobConnector.php');
*/
class AgvWorkshopManager
{
    protected $handler;
  protected $demo;
    function __construct()
    {
      DepartmentPostType::init();
      Checkout\EngagementCart::init();
      database\AgvInstaller::init();
    widgets\AddToCartWidget::init();

      $demo = new demo\AgvDepartmentsDemo();
      $cart = new api\CartController();

        $this->handler = new AgvWorkshopHandler();
        $isValidInstallation = true;
        if (!is_plugin_active('json-rest-api/plugin.php')) {
            add_action('admin_notices', array($this, '_messageUnresolvedDependencies'));
            $isValidInstallation = false;
        }

        if ($isValidInstallation) {
            add_shortcode('agv_workshop_registration', array($this->handler, 'getShortCodeReplacement'));

            add_action('init', 'AgvRegisterPlaceAndTimeTaxonomy', 0);
            add_action('init', 'AgvRegisterWorkshopPosttype', 0);

            add_action('admin_enqueue_scripts', array($this, '_enqueueScripts'));
            add_action('agv_workshops_enqueue_scripts', array($this, '_enqueueScripts'));
            add_action('admin_menu', array($this, '_customizeMenu'));

            add_action('admin_menu', array($this, '_customizeWorkshopEditPage'));
            add_action('add_meta_boxes', array($this, '_addWorkshopLeaderBox'));
          add_action('plugins_loaded', array($this, '_loadTextDomain'));
        }
    }

    function post_listing_page() {
        //this is the wp admin edit.php post listing page!
        ?>
        <h2>Nutzer umbenennen</h2>
        <p>Durch den Aufruf dieser Seite wurden die Anzeigenamen folgender Nutzer geändert:</p>
        <?php
        global $wpdb;
        global $table_prefix;

        //jfPrintDebugArray($workshop);

        $workshopSection = get_option( 'AgvSection4Workshop' );

        $leaders = $wpdb->get_results( "SELECT *
                FROM ". $table_prefix ."users WHERE display_name = user_email" );

        foreach ( $leaders as $leader ) {
            $userObject = get_userdata($leader->ID);
            $lastName = get_user_meta($leader->ID, "last_name", true);
            $surName = get_user_meta($leader->ID, "first_name", true);

            $displayName = trim("$lastName, $surName");
            echo $userObject->user_email;
            echo "-> $lastName, $surName";
            if ($displayName != ","){

                $userObject->display_name = $displayName;
                wp_update_user($userObject);
                echo " -> <b>umbenannt!</b>";
            } else {
                echo " -> nicht umgenannt, da Nachname und Vorname nicht vorhanden waren.";
            }

            echo "<br />";

        }
    }

    function _customizeMenu()
    {
        add_submenu_page('edit.php?post_type=workshops', 'settings', 'Optionen', 'manage_options', 'event_settings', array($this->handler, 'getOptionsContent'));
        add_submenu_page('edit.php?post_type=workshops', 'schedule', 'Plan', 'see_workshop_plan', 'event_schedule', array($this->handler, 'getScheduleContent'));

        add_users_page( "Bulk Rename", "Bulk Rename", "edit_users", "bulk_rename", array($this, 'post_listing_page'));

    }

  function _loadTextDomain(){
    load_plugin_textdomain( 'wp-event-managr', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/mo' );
  }

    function _customizeWorkshopEditPage() {
        remove_meta_box( 'authordiv' , 'workshops' , 'normal' );
    }

    function _addWorkshopLeaderBox(){
        add_meta_box('workshopLeader', 'Workshopleiter', array($this, '_addWorkshopLeaderBoxCallback'), 'workshops', 'side', 'high');
    }

    function _addWorkshopLeaderBoxCallback($workshop){
        global $wpdb;
        global $table_prefix;

        //jfPrintDebugArray($workshop);

        $workshopSection = get_option('AgvSection4Workshop');

        $leaders = $wpdb->get_results("SELECT DISTINCT u.id as ID, concat(u.Nachname, ', ', u.Vorname) as name
                FROM ". $table_prefix ."bereich4ma b
                INNER JOIN ". $table_prefix ."userdata u
                ON b.Ma = u.ID
                WHERE b.BID = ". $workshopSection ." AND b.Jahr = ". get_option('FsmaJahr') ."
                ORDER BY name");

        ?>
        <select name="post_author_override" style="width: 100%">
            <?php
            foreach( $leaders as $leader){
                if ($leader->ID == $workshop->post_author){
                    echo "<option value='". $leader->ID ."' selected='selected'>";
                } else {
                    echo "<option value='". $leader->ID ."'>";
                }
                echo $leader->name;
                echo "</option>";
            }
            ?>
        </select>
        <p>
        <?php

          if (get_user_meta($workshop->post_author, 'community_optin', true) == "1") {
            echo "Ist nicht Teil der Bewegung";
          } else {
            echo "Ist Teil der Bewegung";
          }

        ?>
        </p>
    <?php
    }

    function _messageUnresolvedDependencies()
    {
        echo '<div id="message" class="error fade"><p style="line-height: 150%">';

        _e('<strong>Event Manager</strong></a> requires the WP API plugin to be activated. Please <a href="plugin-install.php?tab=plugin-information&plugin=json-rest-api">install / activate WP API</a> first.', 'event-manager');

        echo '</p></div>';
    }

    function _enqueueScripts()
    {
        $page = get_query_var( 'page', "" );
        echo $page;

        if ($page != "event_schedule"){
           ///Enqueue the dependencies
          wp_enqueue_script('angular', AGV_WORKSHOP_URL . 'app/'. $this->_getRevvedFile('scripts/vendor.js'));

          //Enqueue the app
          wp_register_script('eventManagrApp', AGV_WORKSHOP_URL . 'app/'. $this->_getRevvedFile('scripts/event-managr.js'));
          wp_localize_script('eventManagrApp', 'AGV_WORKSHOP_SETTINGS', array('api' => esc_url_raw(get_json_url()), 'nonce' => wp_create_nonce('wp_json'), 'templateRoot' => AGV_WORKSHOP_URL."app/"));
          // The script can be enqueued now or later.
          wp_enqueue_script('eventManagrApp', array('angular'), '', true);
        } else {
          wp_register_script('eventManagrSchedule', AGV_WORKSHOP_URL . 'app/'. $this->_getRevvedFile('scripts/timetable.js'));
          wp_localize_script('eventManagrSchedule', 'AGV_WORKSHOP_SETTINGS', array('api' => esc_url_raw(get_json_url()), 'nonce' => wp_create_nonce('wp_json'), 'templateRoot' => AGV_WORKSHOP_URL."app/"));

          wp_enqueue_script('eventManagrSchedule', array(), '', true);

        }
        //Enqueue styles
        wp_register_style('event-managr', AGV_WORKSHOP_URL . 'app/'. $this->_getRevvedFile('styles/event-managr.css'));
        wp_enqueue_style('event-managr');

    }

  //Thanks to https://github.com/dennisreimann for this function
  function _getRevvedFile($filename) {
    $json = @file_get_contents(AGV_WORKSHOP_DIR . '/app/rev-manifest.json');
    $rev = json_decode($json, true);
    $revvedPath = $rev[$filename];
    if ($revvedPath) {
      return $revvedPath;
    } else {
      return $filename;
    }
  }
}

$agvWorkshopManager = new AgvWorkshopManager();
$agvUserExtentions = new AgvUserExtentions();
