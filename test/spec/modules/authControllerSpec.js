'use strict';

describe('Controller: authController', function () {

    // load the controller's module
    beforeEach(module('workshopRegApp'));

    var authController,
        scope,
        settings,
        location,
        httpBackend,
        window;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope, $httpBackend, APP_SETTINGS, $location, $window) {
        scope = $rootScope.$new();
        settings = APP_SETTINGS;
        httpBackend = $httpBackend;
        location = $location;
        window = $window;
        authController = $controller('authController', {
            $scope: scope
        });

        spyOn($window.location, 'reload');
    }));

    it('should take the users name', function () {
        expect(scope.name).toBeDefined();
    });


    it('should initial hide the authentication', function () {
        expect(scope.isAuthenticationRequired).toBeFalsy();
    });

    describe('should check for a positive login status for the user', function () {
        beforeEach(function () {
            httpBackend.expectGET(settings.api + 'eventmanagr/people/me').respond(200, {userID: 1});
            httpBackend.flush();
        });

        it('should redirect to user data page', function () {
            expect(location.$$path).toEqual('/data');
        });
    });

    describe('should check a negative the login status for the user', function () {
        beforeEach(function () {
            httpBackend.expectGET(settings.api + 'eventmanagr/people/me').respond(401, {userID: 1});
            httpBackend.flush();
        });

        it('should enable the authentication', function () {
            expect(scope.isAuthenticationRequired).toBeTruthy();
        });
    });

    describe('when a handling authentication request', function () {
        beforeEach(function () {
            httpBackend.expectGET(settings.api + 'eventmanagr/people/me').respond(401, {userID: 1});
        });

        describe('with correct credentials', function () {
            beforeEach(function () {
                httpBackend.expectPOST(settings.api + 'eventmanagr/people/me').respond(200, {user: {ID: 1}});
                scope.user = "horst";
                scope.pw = "1234";
                scope.$valid = true;
                scope.getLogin();
                httpBackend.flush();
            });

            it('should redirect to user data page', function () {
                expect(window.location.reload).toHaveBeenCalled();
                //expect(location.$$path).toEqual('/data');
            });
        });

        describe('with wrong credentials', function () {
            var failed_response = [{"code": "not_authenticated", "message": "You failed"}];
            beforeEach(function () {

                httpBackend.expectPOST(settings.api + 'eventmanagr/people/me').respond(500, failed_response);
                scope.user = "horst";
                scope.pw = "wrong_password";
                scope.$valid = true;
                scope.getLogin();
                httpBackend.flush();
            });

            it('should fill the message with the servers message', function () {
                expect(scope.errors).toEqual(failed_response);
            });
        });
    });
});
