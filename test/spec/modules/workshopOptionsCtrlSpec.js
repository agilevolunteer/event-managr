/**
 * @module test.workshopOptions
 * @name workshopOptionsCtrl
 * @description
 * Tests for workshopOptionsCtrl under workshopOptions
 * _Enter the test description._
 * */


describe('Controller: workshopOptions.workshopOptionsCtrl', function () {

    // load the controller's module
    beforeEach(module('workshopOptionsApp'));

    var ctrl,
        scope,
        service,
        someWeirdOptions;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope, _workshopOptionsService_) {
        scope = $rootScope.$new();
        service = _workshopOptionsService_;
        someWeirdOptions = {
            agvOptions4Workshop: {
                id: 2,
                text: 'I am the Scapman'
            }
        };

        spyOn(service, 'getOptions');
        spyOn(service, 'saveOptions');
        spyOn(service, 'getSections');

        ctrl = $controller('workshopOptionsCtrl', {
            $scope: scope,
            workshopOptionsService: service
        });
    }));

    it('should be defined', function () {
        expect(ctrl).toBeDefined();
    });

    it('should retrieve the options from the service on init', function(){
        expect(service.getOptions).toHaveBeenCalled();
    });

    it('should retrieve the sections from the service on init', function(){
        expect(service.getSections).toHaveBeenCalled();
    });

    it('should save the options through the service', function(){
        scope.options = someWeirdOptions;
        scope.saveOptions();
        expect(service.saveOptions).toHaveBeenCalledWith(someWeirdOptions);
    });
});
