/**
 * @module test.workshopRegApp
 * @name workshopDataCtrl
 * @description
 * Tests for workshopDataCtrl under workshopRegApp
 * _Enter the test description._
 * */


describe('Controller: workshopRegApp.workshopDataCtrl', function () {

    // load the controller's module
    beforeEach(module('workshopRegApp'));

    var ctrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        ctrl = $controller('workshopDataCtrl', {
            $scope: scope
        });
    }));

    it('should be defined', function () {
        expect(ctrl).toBeDefined();
    });
});
