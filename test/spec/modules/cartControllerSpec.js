describe('Controller: checkoutApp.cartController', function(){

  beforeEach(module('eventManagrCart'));

  var ctrl, scope, cartService, cartDefer, fakeCart;

  beforeEach(inject(function($controller, $rootScope, _cartService_, _$q_){
    cartDefer = _$q_.defer();
    scope = $rootScope.$new();
    cartService = _cartService_;
    fakeCart = {
      mainDepartment: {
        name: 'Webmaster',
        desc: 'bla foo weird text'
      }
    };

    spyOn(cartService, 'getCart').and.returnValue({$promise:cartDefer.promise});
    ctrl = $controller('cartController', {
      $scope: scope,
      cartService: cartService
    });
  }));

  it('should be defined', function(){
    expect(ctrl).toBeDefined();
  });

  it('should provide a slot for the chosen main department', function(){
    expect(scope.mainDepartment).toBeDefined();
  });

  it('should provide a slot for a cart', function(){
    cartDefer.resolve(fakeCart);
    scope.$apply();
    expect(scope.cart).toBe(fakeCart);
  });

  it('should have an empty cart if no remote content is available', function(){
    cartDefer.reject();
    scope.$apply();
    expect(scope.cart).toEqual({});
  });

  it('should call the cart on load', function(){
    expect(cartService.getCart).toHaveBeenCalled();
  });

  describe("valid cart handling", function(){
    beforeEach(function(){
      cartDefer.resolve(fakeCart);
      scope.$apply();
    });

    it("should retrieve main department informations from the loaded cart", function(){
      expect(scope.mainDepartment()).toEqual(fakeCart.mainDepartment);
    });
  });

  describe("empty cart handling", function(){
    beforeEach(function(){
      cartDefer.resolve({});
      scope.$apply();
    });

    it("should have empty main department", function(){
      expect(scope.mainDepartment()).toEqual({});
    });
  });

  it("should have an edit mode for users", () => {
    expect(scope.editUser).toBeDefined();
  })

  it("should be in read mode on start", () => {
    expect(scope.editUser).toBeFalsy();

  })
});
