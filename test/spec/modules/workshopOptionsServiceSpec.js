/**
 * @module test.workshopOptionsApp
 * @name workshopOptionsService
 * @description
 * Tests for workshopOptionsService under workshopOptionsApp
 * _Enter the test description._
 * */


describe('Service: workshopOptionsApp.workshopOptionsService', function () {

    // load the service's module
    beforeEach(module('workshopOptionsApp'));

    // instantiate service
    var service,
        $httpBackend,
        settings;

    //update the injection
    beforeEach(inject(function (workshopOptionsService, _$httpBackend_, APP_SETTINGS) {
        service = workshopOptionsService;
        $httpBackend = _$httpBackend_;
        settings = APP_SETTINGS;
    }));

    /**
     * @description
     * Sample test case to check if the service is injected properly
     * */
    it('should be injected and defined', function () {
        expect(service).toBeDefined();
    });

    it('should call an endpoint to retrieve options', function () {
        $httpBackend.expectGET(settings.api + 'eventmanagr/options/workshops').respond(200, {
            someFancyOptions: 1234
        });
        service.getOptions();
        $httpBackend.flush();
    });

    it('should call an endpoint to retrieve possible choices for sections', function () {
        $httpBackend.expectGET(settings.api + 'eventmanagr/sections').respond(200, []);
        service.getSections();
        $httpBackend.flush();
    });

    it('should call an endpoint to save options', function () {
        var someWeirdOptions = {
            agvOptions4Workshop: {
                id: 2,
                text: 'I am the Scapman'

            }
        };
        $httpBackend.expectPOST(settings.api + 'eventmanagr/options/workshops', someWeirdOptions).respond(200, {});
        service.saveOptions(someWeirdOptions);
        $httpBackend.flush();

    });
})
;
