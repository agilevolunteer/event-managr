/**
 * @module test.workshopReg
 * @name WorkshopDataService
 * @description
 * Tests for WorkshopDataService under workshopReg
 * _Enter the test description._
 * */


describe('Service: workshopReg.WorkshopDataService', function () {

    // load the service's module
    beforeEach(module('workshopRegApp'));

    // instantiate service
    var service;

    //update the injection
    beforeEach(inject(function (WorkshopDataService) {
        service = WorkshopDataService;
    }));

    /**
     * @description
     * Sample test case to check if the service is injected properly
     * */
    it('should be injected and defined', function () {
        expect(service).toBeDefined();
    });

    it('should take a plain object for the user', function(){
        expect(service.user).toBeUndefined();
    });
});
