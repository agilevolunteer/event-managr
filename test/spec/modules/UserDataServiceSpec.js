/**
 * @module test.workshopRegApp
 * @name UserDataService
 * @description
 * Tests for UserDataService under workshopRegApp
 * _Enter the test description._
 * */


describe('Service: workshopRegApp.UserDataService', function () {

    // load the service's module
    beforeEach(module('workshopRegApp'));

    // instantiate service
    var service;

    //update the injection
    beforeEach(inject(function (UserDataService) {
        service = UserDataService;
    }));

    /**
     * @description
     * Sample test case to check if the service is injected properly
     * */
    it('should be injected and defined', function () {
        expect(service).toBeDefined();
    });
});
