module Model exposing (..)

import Maybe exposing (Maybe)


type alias TagItem =
    { id : Int
    , label : String
    }


type alias ScheduleItem =
    { id : Int
    , label : String
    , place : List TagItem
    , time : List TagItem
    }


type alias Model =
    { times : List TagItem
    , places : List TagItem
    , workshops : List ScheduleItem
    , editWorkshop : Maybe ScheduleItem
    , chosenWorkshop : Int
    , serverRoot : String
    , message: String
    , nonce : String
    }


initialModel : Model
initialModel =
    { times =
        [ TagItem 0 "Keine Zeit"
        ]
    , places =
        [ TagItem 0 "Kein Ort"
        ]
    , workshops =
        [ ScheduleItem 1 "Why Elm?" [] []
        , ScheduleItem 2 "Building a frontend for self contained systems" [] []
        , ScheduleItem 3 "Something with React" [] []
        , ScheduleItem 4 "JS Stuff" [] []
        , ScheduleItem 5 "Building an event schedule using Elm" [] []
        ]
    , editWorkshop = Nothing
    , chosenWorkshop = -1
    , serverRoot = ""
    , message = ""
    , nonce = ""
    }
