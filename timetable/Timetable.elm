port module TimeTable exposing (..)

import Html.App as Html
import Task
import Model exposing (..)
import Update exposing (..)
import View


--port servingRoot : (String msg) -> Cmd msg
--rootString : Signal Action
--rootString =
--    Signal.map ChangeServerRoot servingRoot


port servingRoot : (String -> msg) -> Sub msg
port nonce : (String -> msg) -> Sub msg


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [servingRoot ChangeServerRoot, nonce ChangeServerNonce]



-- main : Signal Html
--main =
--  StartApp.start
--    { init = Model.initialModel
--    , view = View.view
--    , update = Update.update
--    , inputs = [ servingRoot ]
--    }


init : ( Model, Cmd Msg )
init =
    let
        root =
            initialModel.serverRoot
    in
        ( initialModel
        , Update.getAllData root
        )


main =
    Html.program
        { init = init
        , view = View.view
        , update = Update.update
        , subscriptions = subscriptions
        }
