module View exposing (..)

import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Model exposing (..)
import Update exposing (Msg)
import Maybe exposing (Maybe)
import Debug exposing (..)


entryItem : String -> Html Msg
entryItem time =
    p [] [ text time ]


tableHeadline : TagItem -> Html Msg
tableHeadline item =
    span [ class "place__name" ] [ text item.label ]


workshopItem : ScheduleItem -> Html Msg
workshopItem item =
    div [ class "workshops__item", onClick (Update.ViewWorkshop item) ]
        [ span [] [ text item.label ]
        ]


workshopWithTimeAndPlace : List ScheduleItem -> TagItem -> Html Msg
workshopWithTimeAndPlace workshops tag =
    let
        spot =
            List.filter (\w -> List.member tag w.time) workshops

        className =
            if (List.isEmpty spot) then
                "place__slot place__slot--empty"
            else
                "place__slot"
    in
        div [ class className ] (List.map (workshopItem) spot)


placeRow : List ScheduleItem -> List TagItem -> TagItem -> Html Msg
placeRow workshops times place =
    let
        workshopsInPlace =
            -- TODO: look if place is in list instead of ==
            List.filter (\w -> List.member place w.place) workshops

        --workshops
        workshoplabels =
            List.map (workshopItem) workshopsInPlace

        timeLabels =
            List.map (workshopWithTimeAndPlace workshopsInPlace) times
    in
        div [ class "place__line" ]
            [ tableHeadline place
            , div [ class "workshops" ] (timeLabels)
            ]


timeLabel : Int -> List TagItem -> TagItem -> Html Msg
timeLabel id tagId item =
    --if tagId == item.id then
    if List.member item tagId then
        span [ onClick (Update.EditTimeOfWorkshop id item), class "tag tag--selected" ] [ text item.label ]
    else
        span [ onClick (Update.EditTimeOfWorkshop id item), class "tag" ] [ text item.label ]


placeLabel : Int -> List TagItem -> TagItem -> Html Msg
placeLabel id tagId item =
    --if tagId == item.id then
    if List.member item tagId then
        span [ onClick (Update.EditPlaceOfWorkshop id item), class "tag tag--selected" ] [ text item.label ]
    else
        span [ onClick (Update.EditPlaceOfWorkshop id item), class "tag" ] [ text item.label ]


extractScheduleItem : Maybe ScheduleItem -> List TagItem -> List TagItem -> Html Msg
extractScheduleItem ws t p =
    case ws of
        Just w ->
            (changeView w t p)

        Nothing ->
            text ""


changeView : ScheduleItem -> List TagItem -> List TagItem -> Html Msg
changeView workshop times places =
    div [ class "stage__single" ]
        [ h3 []
            [ text workshop.label ]
        , span [ onClick (Update.CloseEditWorkshop) ] [ text "Schließen" ]
        , label [] [ text "Zeiten:" ]
        , div [ class "tags" ] (List.map (timeLabel workshop.id workshop.time) times)
        , label [] [ text "Orte:" ]
        , div [ class "tags" ] (List.map (placeLabel workshop.id workshop.place) places)
        ]


stylesheet =
    let
        tag =
            "link"

        attrs =
            [ attribute "rel" "stylesheet"
            , attribute "property" "stylesheet"
            , attribute "href" "../app/styles/event-managr.css"
            ]

        children =
            []
    in
        node tag attrs children


view : Model -> Html Msg
view model =
    let
        timeEntries =
            List.map (tableHeadline) model.times

        placesRows =
            List.map (placeRow model.workshops model.times) model.places

        changeView =
            extractScheduleItem model.editWorkshop model.times model.places

        workshopItems =
            List.map workshopItem model.workshops
    in
        div [ class "main stage" ]
            [ div [] [ text model.message ]
            , div [] workshopItems
            , div [ class "stage__schedule" ]
                [ div [ class "times" ] timeEntries
                , div [ class "places" ] placesRows
                ]
            , changeView
            , stylesheet
            , span [ class "server" ]
                [ text "serving from "
                , a [ href model.serverRoot ] [ text (model.serverRoot) ]
                ]
            ]
