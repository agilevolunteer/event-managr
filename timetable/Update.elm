module Update exposing (..)

import Model exposing (Model)
import Http
import Task
import Json.Decode as Json exposing ((:=))


type Msg
    = NoOp
    | ViewWorkshop (Model.ScheduleItem)
    | CloseEditWorkshop
    | EditTimeOfWorkshop Int Model.TagItem
    | EditPlaceOfWorkshop Int Model.TagItem
    | UpdateEditWorkshop (Maybe Model.ScheduleItem)
    | ChangeServerRoot String
    | ChangeServerNonce String
    | FetchTimesSucceed (List Model.TagItem)
    | FetchPlacesSucceed (List Model.TagItem)
    | FetchWorkshopsSucceed (List Model.ScheduleItem)
    | FetchFail Http.Error


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ViewWorkshop item ->
            ( { model | editWorkshop = Just item }, Cmd.none )

        CloseEditWorkshop ->
            ( { model | editWorkshop = Nothing }, Cmd.none )

        UpdateEditWorkshop item ->
            ( { model | editWorkshop = item }, Cmd.none )

        EditTimeOfWorkshop workshopId time' ->
            let
                updateTimeEdit =
                    case model.editWorkshop of
                        Just ws ->
                            Just { ws | time = [time'] }

                        Nothing ->
                            Nothing

                updateTimeSchedule w =
                    if workshopId == w.id then
                        { w | time = [time'] }
                    else
                        w
            in
                ( { model | workshops = List.map updateTimeSchedule model.workshops, editWorkshop = updateTimeEdit }, Cmd.none )

        EditPlaceOfWorkshop workshopId place' ->
            let
                updatePlaceEdit =
                    case model.editWorkshop of
                        Just ws ->
                            Just { ws | place = [ place' ] }

                        Nothing ->
                            Nothing

                updatePlaceSchedule w =
                    if workshopId == w.id then
                        { w | place = [ place' ] }
                    else
                        w
            in
                ( { model | workshops = List.map updatePlaceSchedule model.workshops, editWorkshop = updatePlaceEdit }, Cmd.none )

        ChangeServerRoot root ->
            ( { model | serverRoot = root }
            , getAllData root
            )
        ChangeServerNonce nonce ->
            ( { model | nonce = nonce }
            , Cmd.none
            )

        FetchTimesSucceed times ->
            ( { model | times = Model.TagItem 0 "Keine Zeit" :: times }
            , Cmd.none
            )

        FetchPlacesSucceed places ->
            ( { model | places = Model.TagItem 0 "Kein Ort" :: places }
            , Cmd.none
            )

        FetchWorkshopsSucceed workshops ->
            ( { model | workshops = workshops }
            , Cmd.none
            )

        FetchFail error ->
            let
                note =
                    case error of
                        Http.Timeout -> "Timeout"
                        Http.NetworkError -> "Network Error"
                        Http.UnexpectedPayload s -> s
                        Http.BadResponse status s -> s
            in
            ( { model | message = note}
            , Cmd.none )



--{ model | chosenWorkshop = time' }
--HTTP


getTimes : String -> Cmd Msg
getTimes baseUrl =
    let
        url =
            baseUrl ++ "taxonomies/times/terms"
    in
        Task.perform FetchFail FetchTimesSucceed (Http.get decodeTagItems url)


getPlaces : String -> Cmd Msg
getPlaces baseUrl =
    let
        url =
            baseUrl ++ "taxonomies/places/terms"
    in
        if baseUrl == "" then
            Cmd.none
        else
            Task.perform FetchFail FetchPlacesSucceed (Http.get decodeTagItems url)


getWorkshops : String -> Cmd Msg
getWorkshops baseUrl =
    let
        url =
            baseUrl ++ "posts?type=workshops"
    in
        if baseUrl == "" then
            Cmd.none
        else
            Task.perform FetchFail FetchWorkshopsSucceed (Http.get decodeWorkshops url)


getAllData : String -> Cmd Msg
getAllData baseUrl =
    let
        url =
            baseUrl
    in
        if baseUrl == "" then
            Cmd.none
        else
            Cmd.batch [ getPlaces url, getTimes url, getWorkshops url ]


decodeTagItem : Json.Decoder Model.TagItem
decodeTagItem =
    Json.object2 Model.TagItem
        ("ID" := Json.int)
        ("name" := Json.string)


decodeTagItems : Json.Decoder (List Model.TagItem)
decodeTagItems =
    Json.list decodeTagItem


decodeWorkshops : Json.Decoder (List Model.ScheduleItem)
decodeWorkshops =
    Json.list decodeWorkshop



decodeWorkshop : Json.Decoder Model.ScheduleItem
decodeWorkshop =

    Json.object4 Model.ScheduleItem
        ("ID" := Json.int)
        ("title" := Json.string)
        (Json.at ["terms", "places"] decodeTagItems)
        (Json.at ["terms", "times"] decodeTagItems)
